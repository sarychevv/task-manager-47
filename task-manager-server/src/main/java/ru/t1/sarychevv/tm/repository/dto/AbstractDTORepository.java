package ru.t1.sarychevv.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.repository.dto.IDTORepository;
import ru.t1.sarychevv.tm.comparator.CreatedComparator;
import ru.t1.sarychevv.tm.comparator.StatusComparator;
import ru.t1.sarychevv.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractDTORepository<M extends AbstractModelDTO> implements IDTORepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    private final Class<M> type;

    public AbstractDTORepository(@NotNull final EntityManager entityManager, Class<M> type) {
        this.entityManager = entityManager;
        this.type = type;
    }

    protected Class<M> getTableName() {
        return type;
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) throws Exception {
        entityManager.persist(model);
        return model;
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String jpql = "DELETE FROM " + getTableName().getSimpleName() + " m";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) throws Exception {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final String jpql = "FROM " + getTableName().getSimpleName() + " m";
        return entityManager.createQuery(jpql, getTableName()).getResultList();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) throws Exception {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(getTableName());
        @NotNull final Root<M> from = criteriaQuery.from(getTableName());
        criteriaQuery.select(from);
        criteriaQuery.orderBy(criteriaBuilder.asc(from.get(getSortType(comparator))));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) throws Exception {
        return entityManager.find(getTableName(), id);
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getTableName().getSimpleName() + " m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

    @NotNull
    @Override
    public M removeOne(@NotNull final M model) throws Exception {
        entityManager.remove(model);
        return model;
    }

    @NotNull
    @Override
    public M update(@NotNull final M model) throws Exception {
        entityManager.merge(model);
        return model;
    }

}

